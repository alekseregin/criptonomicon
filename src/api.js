const API_KEY = 'a2ad26cff1383cdd5345090707380f3935c471d6ed0f29f645cbc1fd493dee3a'
const AGGREGATE_INDEX = "5"
const tickersHandlers = new Map()
const socket = new WebSocket(`wss://streamer.cryptocompare.com/v2?api_key=${API_KEY}`)

socket.addEventListener('message', (e) => {
  const { TYPE: type, FROMSYMBOL: currency, PRICE: newPrice } = JSON.parse(e.data)

  if (type !== AGGREGATE_INDEX || newPrice === undefined) {
    return
  }

  const handlers = tickersHandlers.get(currency) || []
  handlers.forEach((fn) => fn(newPrice))
})

const sentToWebSocket = (message) => {
  const stringifiedMessage = JSON.stringify(message)

  if(socket.readyState === WebSocket.OPEN) {
    socket.send(stringifiedMessage)
    return
  }

  socket.addEventListener('open', () => {
    socket.send(stringifiedMessage)
  }, { once: true })
}

const subscribeToTickerOnWs = (ticker) => {
  sentToWebSocket({
    'action': 'SubAdd',
    subs: [`5~CCCAGG~${ticker}~USD`]
  })
}

const unsubscribeToTickerOnWs = (ticker) => {
  sentToWebSocket({
    'action': 'SubRemove',
    subs: [`5~CCCAGG~${ticker}~USD`]
  })
}

export const subscribeToTicker = (ticker, cb) => {
  const subscribers = tickersHandlers.get(ticker) || []
  tickersHandlers.set(ticker, [ ...subscribers, cb])
  subscribeToTickerOnWs(ticker)
}

export const unsubscribeFromTicker = (ticker) => {
  tickersHandlers.delete(ticker)
  unsubscribeToTickerOnWs(ticker)
}
